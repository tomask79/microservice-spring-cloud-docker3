# Writing MicroServices [part 5] #

## Spring Cloud MicroServices in Docker ##

Following repo is the same as in [Part4](https://bitbucket.org/tomask79/microservice-spring-cloud-docker2/overview). But this time I just want to show how you can create basic "bridge" between all nodes so they can see at each other! No manual Docker links are necessary then.

### Docker bridge network ###

Run the following:
```
docker network create spring-cloud-network
```
Since we've got a network now, let's put all of the nodes on that...

### Eureka Server ###

Maven compilation and image building is same as in Part 4. 
For launching eureka container, use:

```
docker run -i -t --name eureka --net spring-cloud-network -p 9761:971 registry/demo
```
(--name parameter creates DNS entry on the spring-cloud-network)

### MicroService ###

Maven compilation and image building is same as in Part 4. 
For launching MicroService container, use:

```
docker run -it --name service --net spring-cloud-network service/demo
```

### Gateway client ###

Maven compilation and image building is same as in Part 4. 
For launching client container, use:

```
docker run -it --name client --net spring-cloud-network client/demo
```
(output should be as in Part 4)

### Everyone see at each other! ###

For example, let's pin yourself to the service container:

```
docker exec -it service bash
60623256:/# ping eureka
PING eureka (172.19.0.2): 56 data bytes
64 bytes from 172.19.0.2: icmp_seq=0 ttl=64 time=0.085 ms
64 bytes from 172.19.0.2: icmp_seq=1 ttl=64 time=0.100 ms
64 bytes from 172.19.0.2: icmp_seq=2 ttl=64 time=0.110 ms
```
or 
```
t@d18960623256:/# ping client
PING client (172.19.0.4): 56 data bytes
64 bytes from 172.19.0.4: icmp_seq=0 ttl=64 time=0.110 ms
64 bytes from 172.19.0.4: icmp_seq=1 ttl=64 time=0.106 ms
64 bytes from 172.19.0.4: icmp_seq=2 ttl=64 time=0.089 ms
```

### Summary ###

If you're fine with topology that every container see at each other, basic docker bridge network is fine, but keep in mind that **it works only at one host**. For multiple hosts, you'd need to use Overlay network.

see you

Tomas